package com.shunyi.knowit.redislearning;

import com.shunyi.knowit.redislearning.util.RedisUtil;

/**
 * @author esehhuc
 * @create 2021-06-03 13:22
 */
public class Demo {
    public static void main(String[] args) {
        RedisUtil redisUtil = new RedisUtil();
        //通过util操作
        redisUtil.set("k1", "abc");
        System.out.println(redisUtil.get("k1"));
    }
}
