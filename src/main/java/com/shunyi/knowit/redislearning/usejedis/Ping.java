package com.shunyi.knowit.redislearning.usejedis;

import com.alibaba.fastjson.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * @author Shunyi Chen
 * @create 2021-04-10 21:55
 */
public class Ping {

    private static final String HOST = "192.168.3.249";
    private static final int PORT = 6379;

    public static void main(String[] args) {
        Jedis jedis = new Jedis(HOST, PORT);
        String pong = jedis.ping();
        System.out.println(pong);
        //Test with bitmap
        jedis.pfadd("signIn", new String[]{"星期日", "0"});
        jedis.pfadd("signIn", new String[]{"星期一", "1"});
        jedis.pfadd("signIn", new String[]{"星期二", "1"});
        jedis.pfadd("signIn", new String[]{"星期三", "1"});
        jedis.pfadd("signIn", new String[]{"星期四", "1"});
        jedis.pfadd("signIn", new String[]{"星期五", "1"});
        jedis.pfadd("signIn", new String[]{"星期六", "0"});
        long count = jedis.pfcount("signIn", "星期一");
        System.out.println(count);
        //Test with transaction
        JSONObject obj = new JSONObject();
        obj.put("userName", "Shunyi");
        obj.put("password", "22334");
        Transaction trans = jedis.multi();
        String json = obj.toJSONString();
        try {
            trans.set("user1", json);
            trans.set("user2", json);
            trans.exec();
        } catch (Exception e) {
            trans.discard();
            e.printStackTrace();
        } finally {
            String val1 = jedis.get("user1");
            System.out.println(val1);
            String val2 = jedis.get("user2");
            System.out.println(val2);

            jedis.close();
        }

        jedis.close();
    }
}
