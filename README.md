# 注意： # 

## 1.Redis单条命令是保证原子性的，但事务不保证原子性 ## 

## 2.解决jedis访问问题
```sh
--> systemctl stop firewalld  
--> systemctl disable firewalld  
--> 更改redis.conf添加bind 0.0.0.0,重启redis服务，参考https://www.cnblogs.com/xdyixia/p/9057019.html  
```

# 1.安装Redis

```sh
yum install gcc-c++
cd redis-6.0.10
make
make install

cd /usr/local/bin
mkdir myconfig
cp redis.conf /usr/local/bin/myconfig
cd myconfig
vim redis.conf
change 'deamonize' to yes

//Startup redis server
redis-server myconfig/redis.conf

//Startup client
redis-cli -p 6379

ping
```

# 2.基本命令

```sh
keys *
set name shunyi
get name
exists name
move name 0
flushdb
flushall
redis-benchmark -h localhost -p 6379 -c 100 -n 10000
EXPIRE name 10
ttl name
type name
```

# 3.String 类型

```sh
set name "shunyi"
append name " chen"
strlen name
set value 0
incr value 自增1
decr value 自减1
incrby value 10 自增10
decrby value 10 自减10
set key1 "Hello, Shunyi"
getrange key1 0 3
getrange key1 0 -1 取全字符串
setrange key1 1 xxxx  替换1位置字符串
setex name 30 "hello"  30秒后过期删除
ttl name 查看剩余时间
setnx name "Shunyi"  当name不存在可以设置,用于实现分布式锁
set name 666 nx ex 10  等同setnx，但加timeout
msetnx k1 "v1" k2 "v2"
msetnx k1 "v1" k4 "v4" 返回0，因为是原子性操作要么一起成功要么一起失败
get k4 返回nil
set user:1  {name:shunyi,age:35}
mset user:1:name shunyichen user:1:age 38
mget user:1
mget user:1:name user:1:age
getset db redis 先取旧值再设置新值
getset db mongodb
```

# 4.List类型

```sh
LPUSH list one
LPUSH list two
LRANGE list 0 -1 全部取出来  倒着遍历
LRANGE list 0 1 取部分集合  倒着遍历
RPUSH list one
RPUSH list two
LRANGE list 0 -1 全部取出来  正着遍历
LPOP list   弹出最左的元素
RPOP list  弹出最右的元素
LINDEX list 1 通过下标获取值
LLEN list 获取list长度
LREM list 1 one 移除list中1个one
```









```c
Please ignore the following content
```


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact